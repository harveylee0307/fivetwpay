'use strict'
$(function () {
    function a() {
        $('.faqCont .faqQuestion').removeClass('active'), $('.faqCont .faqAnswer').slideUp(300)
    }
    $(window).on('scroll', function () {
        100 < $(window).scrollTop()
            ? $('header').addClass('scrolling')
            : $('header').removeClass('scrolling')
    })
    $(window).on('scroll', function () {
        200 < $(window).scrollTop() ? $('.mediaIcons').fadeIn(300) : $('.mediaIcons').fadeOut(300)
    })
    $(window).on('scroll', function () {
        600 < $(window).scrollTop() ? $('.goTop').fadeIn(300) : $('.goTop').fadeOut(300)
    })
    $('.goTop').on('click', function () {
        $('body, html').animate({ scrollTop: 0 }, 400)
    })
    $('.faqQuestion').on('click', function (o) {
        var t = $(this).attr('href')
        $(o.target).is('.active')
            ? a()
            : (a(), $(this).addClass('active'), $('.faqCont ' + t).slideDown(300)),
            o.preventDefault()
    })
    $('.thin .penal__title').on('click', function () {
        $(this).parent().toggleClass('active')
        $(this).next('.penal__content').slideToggle('500')
    })
    // click scroll to href
    $('.scrollTo').on('click', function (e) {
        e.preventDefault()
        var target = $(this).attr('href')
        $('html, body').animate({ scrollTop: $(target).offset().top - 80 }, 1000)
    })
    new WOW().init()
})
